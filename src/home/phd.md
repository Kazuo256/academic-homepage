
### PhD Research

We propose The Unlimited Rulebook, a reference architecture for games with
heavy use of economy mechanics such such as role playing games, card games,
strategy games, tycoons, sandbox games, among others. [Read more!]($(url
"research"))


