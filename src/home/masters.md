
### Masters Thesis

We studied the challenges of real-time soundtracks in games and developed
VORPAL, a middleware for procedural audio based on
[PureData](https://puredata.info/). The research incuded the production of a
proof-of-concept game, *Sound Wanderer*. [Read more!]($(url
"research/masters"))

