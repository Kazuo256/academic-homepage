
### Games Development

Since my freshman year, when I helped found USPGameDev, I have studied game
programming and design while also developing a number of games, both digital and
analogue. Nowadays, I regularly participate on game jams such as [Global Game
Jam](http://globalgamejam.org/) and [Ludum Dare](https://ldjam.com/). You can
see all of the games I have worked on in the [projects
section]($(url 'projects/index.html')). Along the years, I have given lectures
on game development at University of São Paulo, University of Campinas, and the
Brazillian Campus Party; besides organizing [University of São Paulo's site for
Global Game Jam 2017 (site in
portuguese)](http://www.interlab.pcs.usp.br/ggj17/).

<br/>

### Software Projects

Besides games, there are a few other projects I have participated in or
developed on my own. Many of them are actually related to game development
- such as the [UGDK game engine](https://github.com/uspgamedev/ugdk) - while
others delve into more or less different areas of computer science. For
instance, I mantain a very limited [Lua](http://www.lua.org/) extension
distribution called [LUX](https://github.com/Kazuo256/luxproject). This site, on
the other hand, was made using my own static HTML generator,
[Compendium](https://github.com/kazuo256/compendium). The source for this page
is available [here](https://gitlab.com/Kazuo256/academic-homepage).

<br/>

