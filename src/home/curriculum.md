
### Doctoral Research <small>2017 - ???</small>

I'm studying the many ways in which the discipline of software atchitecture can
improve the development of digital games. You can find out more in the [research
section]($(url 'research/index.html')).

<br/>


### C.S. Graduate <small>2009 - 2013</small>

I graduated from University of São Paulo as well. Among the noteworthy events
of my graduation, I've helped found a student-managed research group called
USPGameDev, which aims to gather and spread knowledge on the development of
games.

<br/>

