
### *Backdoor Route* <small style="display:inline-block">2016 – now</small>

One of the long-standing projects I participate in is a game from USPGameDev
called [Backdoor Route](https://uspgamedev.itch.io/backdoor-route). It is a
role-playing game where your abilities, items, and power ups all come from
cards you must skillfully play while traversing a hostile, crumbling planet.
Most importantly, though, this game is part of [my research]($(url "research"))
— I am using it as a case study on software architecture in games.

