
### USPGameDev <small style="display:inline-block">2009 – now</small>

Although not a specific project *per se*, USPGameDev is nevertheless a major
undertaking in my academic career. We research and [develop
games](https://uspgamedev.itch.io), organize events, [produce didactic material
(mostly in Portuguese)](https://usp.game.dev.br/wiki/Referências), among other
activities. USPGameDev hopes to provide a welcoming environment where students
(and non-students) can study game development to complement their formation.

We also defend the use and [production of free / open source
software](http://github.com/uspgamedev), having partnered with USP's [FLOSS
Competence Center (CCSL)](http://ccsl.ime.usp.br/) several times. You are
always welcome to attend the [Global Game Jam](https://globalgamejam.org) site
we host with help from both the CCSL and the *Instituto de Matemática e
Estatística* every year.

