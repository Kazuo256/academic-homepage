
### Problem domain

My PhD project studies the many ways in which the discipline of software
architecture can improve the development of digital games. I am particularly
interested in the challenges of implementing what I refer to as *economy
mechanics*. They are gameplay features that allow the player to manage
resources *of any kind* inside the virtual world of the game.

Coins, items, cards, units, character statistics, skills, magic, time, as well
as all the various interactions among them — any resource that plays a part is
game.

