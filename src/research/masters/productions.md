
### Publications

Wilson K. Mizutani. [VORPAL: A Middleware for Real-Time Soundtracks in Digital
Games](https://www.ime.usp.br/~kazuo/masters/ThesisWilsonMizutani-Revised.pdf).
Masters thesis. *Instituto de Matemática e Estatística*, University of São
Paulo, 2017.

Wilson K. Mizutani and Fabio Kon. [VORPAL: An Extensible and Flexible
Middleware for Real-Time Soundtracks in Digital
Game](https://www.ime.usp.br/~kazuo/masters/cmmr2016-mizutani-kon.pdf). In
*Proceedings of the 12th International Symposium on CMMR*, pages 175–182. The
Laboratory of Mechanics and Acoustics, 2016.

### Developed Software Systems

+ [VORPAL middleware](https://github.com/vorpal-project/vorpal)
+ [Sound Wanderer](https://gitlab.com/Kazuo256/sound-wanderer)

