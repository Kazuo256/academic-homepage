
During my Masters research, I investigated the challenges of real-time
soundtracks in digital games, from which the [VORPAL
project](http://compmus.ime.usp.br/en/vorpal) was born - a middleware for
real-time soundtracks in games. It was a joint project between the Distributed
Systems and Computer Music research groups of the C.S. department here at IME.

For the evaluation of the VORPAL middleware, we developed the concept-of-proof
game called *Sound Wanderer* (see video) in a joint project with sound designer
Dino Vicente.


