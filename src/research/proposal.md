
### Research proposal $(icon "media/anvil-impact.png")

To reduce the development costs and efforts of implementing economy mechanics,
especially in in games with complex resource interactions, my research project
proposes a *reference architecture* called The Unlimited Rulebook. It describes
how to determine the software requirements for economy gameplay and how to
incorporate that into the game architecture.

As part of this research, we are also involving a couple of actual games
developed by USPGameDev so we can evaluate the efficacy of our design. You
can learn more about them in the [projects section]($(url "projects")).

