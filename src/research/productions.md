
### Publications $(icon "media/bookshelf.png")

Wilson K. Mizutani. [The Unlimited Rulebook: Architecting the Economy Mechanics
of Games](https://www.ime.usp.br/~kazuo/thesis/Mizutani-The_Unlimited_Rulebook.pdf).
Ph.D. Thesis. University of São Paulo, October 2021.

Wilson K. Mizutani, Vinícius K. Daros, and Fabio Kon. [Software architecture
for Digital Game Mechanics: a Systematic Literature
Review](https://doi.org/10.1016/j.entcom.2021.100421) ([alternate
link](https://www.ime.usp.br/~kon/papers/SoftwareArchitectureForDigitalGameMechanics2021.pdf)).
Entertainment Computing, Volume 38, 100421. Elsevier, May 2021.

Wilson K. Mizutani and Fabio Kon. [Unlimited Rulebook: a Reference
Architecture for Economy Mechanics in Digital Games]($(url
"media/ArtigoWilsonICSA2020.pdf")). In *Proceedings of the 2020 IEEE
International Conference on Software Architecture (ICSA)*, pages 58–68. IEEE,
2020.

Wilson K. Mizutani and Fabio Kon. [Toward a reference architecture for economy
mechanics in digital
games](https://www.sbgames.org/sbgames2019/files/papers/ComputacaoShort/197859.pdf).
In *Proceedings of the Brazilian Symposium on Games and Digital Entertainment
(SBGames 2019)*, pages 623–626. SBC, 2019.

