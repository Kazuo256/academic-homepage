
-- luacheck: globals printPage render

local PREFIX = "/~kazuo/"

local sections = {
  {'home', 'Home'},
  {'research', 'Research'},
  {'projects', 'Projects'}
}

local pages = {
  'home',
  'research',
  'projects',
  'research/masters'
}

local function url (path)
  return PREFIX .. path
end

for i=1,#pages do
  local page = pages[i]
  local path = page .. '/index'
  printPage(path, render 'layout' {
    url = url,
    current_page = page:match("^(.+)/") or page,
    print = print,
    sections = sections,
    page_content = render (path) { url = url, print = print }
  })
end

